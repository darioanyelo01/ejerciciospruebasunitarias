/*
Nª1
Crear la función que reciba un arreglo de números y retorne el promedio de la suma de todos los números, además se deberá crear los test
*/
function calcularPromedio(arregloDeNotas){
    let sumaDenotas = 0;
    
    arregloDeNotas.forEach(function(valorActual,indece){
        sumaDenotas=valorActual+sumaDenotas;
    });
    const cantidadnotas = arregloDeNotas.length;
    const promedio = sumaDenotas/cantidadnotas;
    console.log('El promedio es:'+ promedio)
    
}
    calcularPromedio([10,20,30,15]);
    
/*
Nª2
Hacer una función que reciba 2 parametros (nombres y apellidos) y permita concantenarlas y convertirlas a mayusculas, además se deberá crear el test
 */
function crearNombre(nombre,apellido){
    
    return (nombre + " " + apellido).toUpperCase();
}
  const nomb = crearNombre("luz","bardarles")
  console.log(nomb);

/*
Nª3
Crear una función para saber si una persona es mayor de edad, esta función recibirá un parametro edad de tipo número, además se deberá crear los test
 */
function mayorEdad(edad){

    if(edad>=18){
        return (`La edad es ${edad}, entonces es Mayor`)
    }else{
        return (`La edad es ${edad}, entonces es Menor`)
    }
}
    const ed = mayorEdad(18)
    console.log(ed);

/*
Nª4
Crear una función para saber si una persona es mayor de edad, esta función recibirá un parametro edad de tipo número, además se deberá crear los test
 */    
function MyNum(numero){
    var al = numero.length;
    maximum = numero[al-1];
    while (al--){
        if(numero[al] > maximum){
            maximum = numero[al]
        }
    }
            
     return ('El numero mayor es: '+ maximum);
};

   const num = MyNum([10,20,30,15,45]);
    console.log(num);

/*
Nª5
Crear una función FIZZBUZZ y hacer unit test
 */  

function fizzbuzz(){
    
    for(let i=1; i <= 100; i++)
    {
        if(i % 3 == 0 && i % 5 == 0){
            return ("fizzbuzz");
        }else if(i%3==0){
            return("fizz");
        }else if(i%5==0){
            return("buzz");
        }else{
            return(i);
        }
    }
}

console.log(fizzbuzz());

module.exports = { calcularPromedio, crearNombre, mayorEdad, MyNum };