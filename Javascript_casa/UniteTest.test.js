
const { calcularPromedio } = require('./UniteTest');

test('El calculo promedio seria',()=> {
    expect(calcularPromedio([10,20,20,15])).toBe(16.25);
});

const { crearNombre } = require('./UniteTest');

test('El nomre y el apellido seria en Mayuscula',()=> {
    expect(crearNombre("luz", "bardarles")).toBe("LUZ BARDARLES");
});

const { mayorEdad } = require('./UniteTest');

test('deberia se mayor si su edad es más de 18',()=> {
    expect(mayorEdad(18)).toBe("Es mayor de edad");
});

const { MyNum } = require('./UniteTest');

test('deberia ser el mayor de los numeros',()=> {
    expect(MyNum(10,20,30,15,45)).toBe(45);
});







